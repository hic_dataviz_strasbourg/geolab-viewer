#!/usr/bin/env python

import Tkinter as tk
import matplotlib
matplotlib.use('TkAgg')
from mpl_toolkits.mplot3d import Axes3D
import mysql.connector
import time
import threading
import datetime

import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import matplotlib.pyplot as plt
# implement the default mpl key bindings
#%% import data from csv (coordinates wells)
path_GRT1 = "/home/hyamanieu/Documents/Hackathon/GeoLab Viewer/traj_GRT1.csv"
GRT1_xyz = np.loadtxt(open(path_GRT1,"rb"),delimiter=' ',skiprows=1)

path_GRT2 = "/home/hyamanieu/Documents/Hackathon/GeoLab Viewer/traj_GRT2.csv"
GRT2_xyz = np.loadtxt(open(path_GRT2,"rb"),delimiter=' ',skiprows=1)
#%%

class Geolab_app(tk.Frame):
    def __init__(self, master = None):
        win = tk.Tk()
        win.geometry("600x600+20+20")
        tk.Frame.__init__(self, master=win)
        self.grid(column=0, row=0, sticky=(tk.N,tk.W))
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.createWidgets()
        self.makeCanvas()
        
    def importWell(self):
        "function to import well data"
        pass
    
    def connectSCDB(self):
        "function to connect to the seiscomp database"
        conn = mysql.connector.connect(host="localhost",user="root", database="seiscomp_hackathon")
        cursor = conn.cursor()
        date1 = "1999-12-31 04:21:26"
        date2 = "1999-12-31 21:21:26"
        cursor.execute('SELECT T3.Time_value, T3.Latitude_value, T3.Longitude_value,T3.depth_value, T4.Magnitude_value FROM (SELECT E.PreferredOriginID, T1.Time_value, T1.Time_value_ms, T1.Latitude_value, T1.Longitude_value,T1.depth_value, E._oid FROM Event AS E INNER JOIN (SELECT O.Time_value, O.Time_value_ms, O.Latitude_value, O.Longitude_value,O.depth_value,  PO.PublicID FROM Origin AS O INNER JOIN PublicObject AS PO ON O._oid = PO._oid) AS T1 ON T1.PublicID = E.PreferredOriginID) AS T3 INNER JOIN (SELECT T2.Magnitude_value, E.PreferredMagnitudeID, E._oid FROM (SELECT M.Magnitude_value, PO.PublicID FROM Magnitude AS M INNER JOIN PublicObject AS PO ON M._oid = PO._oid) AS T2 INNER JOIN Event AS E ON T2.PublicID = E.PreferredMagnitudeID) AS T4 ON T3._oid = T4._oid WHERE T3.Time_value BETWEEN %d AND %d ;',date1, date2)
        rows = cursor.fetchall()
        tab = np.array(rows)
        tab= tab.transpose()
        cursor.close()
        sP = (tab[4]-tab[4].min()+1)*10
        self.a.set_xlim3d(GRT1_xyz[:,0].min()-0.02,GRT1_xyz[:,0].max()+0.02)
        self.a.set_ylim3d(GRT1_xyz[:,1].min()-0.02,GRT1_xyz[:,1].max()+0.02)
        self.a.set_zlim3d(-GRT2_xyz[:,2].max()-1000,-GRT2_xyz[:,2].min())
        self.a.scatter(tab[1],tab[2],-tab[3]*1000,s=sP, c='r', marker='o')
        plt.show()
        self.canvas.draw()
        self.canvas.show()
        
    
    def connectTSCDB(self):
        "function to connect to the time series database"
        pass
    
    
    def makeCanvas(self):
        self.Frame3D=tk.Frame(self.master, background='white')
        self.Frame3D.grid(column=1,row=0)
#        self.FrameToolbar = tk.Frame(self.master, background='white')
#        self.FrameToolbar.grid(column=1,row=1)
        self.f = plt.Figure(figsize=(4,5),dpi=100)
        self.canvas = FigureCanvasTkAgg(self.f,master=self.Frame3D)
        self.a = Axes3D(self.f)
#        self.a = self.f.add_subplot(111, 
#        projection='3d',
#        )
        self.a.format_coord = lambda x,y: ''
        self.a.plot(GRT1_xyz[:,0],GRT1_xyz[:,1],-GRT1_xyz[:,2], label='well 1')
        self.a.plot(GRT2_xyz[:,0],GRT2_xyz[:,1],-GRT2_xyz[:,2], label='well 2')
        
        
        self.a.grid(False)
        self.canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH)
        self.canvas._tkcanvas.pack(side=tk.TOP,pady=10,padx=10, 
                                   fill=tk.BOTH, expand= 1)
        
#        self.toolbar = NavigationToolbar2TkAgg( self.canvas, self.Frame3D)
#        self.toolbar.update()
#        self.toolbar.pack()
        plt.show()
        self.canvas.draw()
        self.canvas.show()
            
        
        
    def createWidgets(self):
        #def import well button
        self.importWell_B = tk.Button(self, text="Import well", 
                                      command=self.importWell)
        self.importWell_B.grid(column=0, row=0, sticky=(tk.W, tk.E))
        #def connect to seiscomp database button
        self.connectSCDB_B = tk.Button(self, text="connect seiscomp.DB", 
                                       command=self.connectSCDB)
        self.connectSCDB_B.grid(column=0, row=1, sticky=(tk.W, tk.E))
        #def connect to time series database button
        self.connectTSCDB_B = tk.Button(self, text="connect TimeSeries.DB", 
                                    command=self.connectTSCDB)
        self.connectTSCDB_B.grid(column=0, row=2, sticky=(tk.W, tk.E))        
        
        self.startDate_txt = tk.Label(self, text="Start date: (YYYY-MM-DD HH:MM:SS)")
        self.startDate_txt.grid(column=0,row=3)
        self.endDate_txt = tk.Label(self, text="End date:")
        self.startDate_ = tk.Entry(self, text="Enter start date")
        self.startDate_.grid(column=0, row=4)
        self.endDate_txt.grid(column=0,row=5)
        self.endDate_ = tk.Entry(self, text="Enter end date")
        self.endDate_.grid(column=0, row=6)

       
        
        self.quitButton = tk.Button(self, text='Quit',
            command=self.master.destroy)
        self.quitButton.grid(column=0, row=8, sticky=(tk.W, tk.E))
        
        
        
        
app = Geolab_app()
app.master.title("GeoLab Viewer")

app.mainloop()